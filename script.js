console.log(`S18 Activity`)
//4
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friend: [
		{name:"xiao", town:"liyue"},
		{name:"mona", town:"monstad"},
		{name:"ganyu", town:"liyue"},
		{name:"raiden shougun", town:"inazuma"},
		{name:"yae miko", town:"linazuma"}
	],
	talk: function(){
		//5
		console.log(`${this.pokemon[0]}! I choose you!`)
	}
}
console.log(trainer)
//6
console.log(`Result of dot notation:`)
console.log(trainer.name)
console.log(`Result if bracket notation:`)
console.log(trainer['pokemon']);
//7
console.log(`result of talk method`)
trainer.talk()
//8
function Pokemon(name,lvl,hp,atk){
	this.name = name
	this.lvl = lvl
	this.hp = hp
	this.atk = atk
	//10
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		target.hp -= this.atk
		console.log(`${target.name} health is now ${target.hp}`)
		this.faint(target)
		console.log(target)
	}
	//11
	this.faint = function(target){
		//12
		if(target.hp<=0){
			console.log(`${target.name} has fainted`)
		}
	}
}
//9
let pikachu = new Pokemon("Pikachu",12,24,12)
let geodude = new Pokemon("Geodude",8,16,8)
let mewto = new Pokemon("Mewto",100,200,100)
//13 invoke tackle
geodude.tackle(pikachu)
mewto.tackle(geodude)